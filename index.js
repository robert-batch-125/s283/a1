let http = require("http");

let PORT = 3000;

http.createServer( (req, res) => {

	if(req.url == "/login"){
		res.writeHead(200,
			{"Content-Type": "text/html"});
			res.write("Welcome to Login Page");
			res.end();
	} else {
		res.writeHead(404,
			{"Content-Type": "text/plain"});
		res.end("Error: Page not found");
	}

} ).listen(PORT);

console.log("Server is successfully running");